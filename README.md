# Seance

Seance is a Python3 program built to automatically generate PDF assessment reports from .json files exported from SpecterOps' Ghostwriter platform.
This program has only been tested on Kali (2020.1).

## Installation

This program requires Pandoc, TeX Live, the Eisvogel Pandoc LaTeX PDF Template, and the python MdUtils module to function.

Run the following command to install Pandoc:

```bash
apt-get update; apt-get -y upgrade; apt-get install pandoc texlive texlive-fonts-extra
```

Run the following command to download the Eisvogel Pandoc LaTex PDF Template:

```bash
git clone https://github.com/Wandmalfarbe/pandoc-latex-template.git
```
Then, move the `eisvogel.tex` file to your pandoc templates folder and rename the file to `eisvogel.latex`.
The location of the templates folder in Kali is `/usr/share/pandoc/data/templates/`.

Run the following command to install the MdUtils python module:

```bash
pip3 install mdutils
```

## Usage

1. Drop your .json reports from Ghostwriter into the `src` directory in the Seance folder.
2. Write a `summary.txt` in the `src` directory with a short overall summary of the assessment.
3. Run the command `./seance.py` and follow the instructions.
4. ???
5. Profit.
