#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import os
import sys
import json
import argparse
from datetime import date
from mdutils.mdutils import MdUtils

input('This script will generate a .md file and a .pdf file from Ghostwriter .json reports.\n'
	  'Before we start, place your .json reports in a directory named \'src\' in your reporting directory.\n'
	  'This directory should be empty except for the .json reports you want generated and the \'summary.txt\' file.\n'
	  'Be aware, at this time, this script can only take one report per project type at a time.\n'
	  'This script accepts the following project types:\n'
	  ' - External Penetration Test\n'
	  ' - Internal Penetration Test\n'
	  ' - Phishing Assessment\n'
	  ' - Red Team Assessment\n'
	  ' - Web Application Assessment\n'
	  ' - Wireless Network Assessment\n'
	  'Press Enter when ready...')

# Configure source directory
config = {}
src_dir = ""
if os.path.isfile('./resources/config.json'):	# Change this if moving seance.py into /usr/bin
	config = open('./resources/config.json')	# Change this if moving seance.py into /usr/bin
	config = json.load(config)
else:
	print('Could not find the config file. Did you delete the config file?')
	sys.exit()

if config['src_dir'] and (config['src_dir'][-1] != '/') and (config['src_dir'][-3:] == 'src'):
	src_dir = config['src_dir']
else:
	config['src_dir'] = input('Source directory not configured. Input the path to your \'src\' directory (e.g. /path/to/src): ')
	if not os.path.isdir(config['src_dir']):
		print(config['src_dir'] + ' is not a valid directory!')
		sys.exit()
	if config['src_dir'][-1] == '/':
		print('Path must not end with a \'/\'!')
		sys.exit()
	if config['src_dir'][-3:] != 'src':
		print('This is not the \'src\' directory!')
		sys.exit()
	src_dir = config['src_dir']
	with open('./resources/config.json', 'w') as f:
		json.dump(config, f)

print('Source directory is set as "' + src_dir + '"')

summary = ""
if not os.path.isfile(src_dir + '/summary.txt'):
	print('The \'summary.txt\' file is missing from the \'src\' directory!')
	sys.exit() 

with open(src_dir + '/summary.txt', 'r') as sum_file:
	summary = sum_file.read()

#debug
#src_dir = '/home/michael/pentest/reporting/src'

outfile_name = input('Enter the name you want the files saved as (e.g. report): ')
if not os.path.isdir(src_dir + '/../out'):
	os.mkdir(src_dir + '/../out')
outfile = src_dir + '/../out/' + outfile_name

#debug
#outfile = src_dir + '/../out/report'

evidence_dir = src_dir + '/evidence'
server = 'https://ghostwriter.local/'
pattern = r'\<.*?\>'
bad_nl= r'\r\n'

cmd = ('pandoc %s.md -o %s.pdf '
	   '--from markdown+yaml_metadata_block+raw_html '
	   '--template eisvogel '
	   '--table-of-contents '
	   '--toc-depth 6 '
	   '-V papersize:"letter" '
	   '--number-sections '
	   '--top-level-division=chapter '
	   '--highlight-style breezedark' % (outfile, outfile))

reports = {}

client = {}
oversight = {}
lead = {}

# Finds .json files in provided directory and adds them to appropriate dictionary
for root, dirs, files in os.walk(src_dir):
	for file in files:
		if file.endswith('.json'):
			temp = open(src_dir + '/' + file)
			temp = json.load(temp)
			if not client:
				client = temp['client']
			if not lead:
				for i in temp['team']:
					if temp['team'][i]['project_role'] == "Assessment Oversight":
						oversight = temp['team'][i]
					if temp['team'][i]['project_role'] == "Assessment Lead":
						lead = temp['team'][i]
			project_type = temp['project']['project_type']
			if project_type == 'External Penetration Test':
				reports['ext_test'] = temp
			if project_type == 'Internal Penetration Test':
				reports['int_test'] = temp
			if project_type == 'Wireless Network Assessment':
				reports['wireless'] = temp
			if project_type == 'Web Application Assessment':
				reports['webapp'] = temp
			if project_type == 'Red Team Assessment':
				reports['redteam'] = temp
			if project_type == 'Phishing Assessment':
				reports['phish'] = temp

risk_levels = {}
for key in reports:
	risk_levels[key] = input('What was the risk level determined by the ' + reports[key]['project']['project_type'] + ' [ Low / Medium / High ] ? ')
	while risk_levels[key] not in ['Low', 'Medium', 'High']:
		risk_levels[key] = input('Invalid input. Please provide a valid risk level for the ' + reports[key]['project']['project_type'] + ' [ Low / Medium / High ] ? ')

# Pulls out main POC
main_poc = {}
for i in client['poc']:
	if client['poc'][i]['note'] != "":
		main_poc = client['poc'][i]

# Creates evidence directory
if not os.path.isdir(evidence_dir):
	os.mkdir(evidence_dir)

# Creates evidence subdirectories and wget's images from Ghostwriter
for key in reports:
	evidence_subdir = evidence_dir + '/' + key
	if not os.path.isdir(evidence_subdir):
		os.mkdir(evidence_subdir)
	for key2 in reports[key]['findings']:
		key3 = next(iter(reports[key]['findings'][key2]['evidence']))
		if os.path.isfile(evidence_subdir + '/' + os.path.split(reports[key]['findings'][key2]['evidence'][key3]['url'])[1]):
			print(os.path.split(reports[key]['findings'][key2]['evidence'][key3]['url'])[1] + ' is already in ' + evidence_subdir)
		else:
			print('Downloading ' + key + '/' + os.path.split(reports[key]['findings'][key2]['evidence'][key3]['url'])[1])
			wget_cmd = 'wget -q --no-check-certificate -P ' + evidence_subdir + ' ' + server + reports[key]['findings'][key2]['evidence'][key3]['url']
			os.system(wget_cmd)

today = date.today()

# Create MD file
f = MdUtils(file_name = outfile)

# Title Page
f.new_line('---')
f.new_line('title: "Security Assessment Report for %s"' % client['full_name'])
f.new_line('author: ["%s", "%s"]' % (lead['name'], lead['email']))
f.new_line('date: %s' % today)
f.new_line('subject: "Security Assessment"')

keywords = 'keywords: ['
for key in reports:
	keywords += reports[key]['project']['codename'] + ', '
keywords += client['codename'] + ']'

f.new_line(keywords)
f.new_line('subtitle: "%s"' % client['codename'])
f.new_line('lang: "en"')
f.new_line('titlepage: true')
f.new_line('titlepage-color: "FFFFFF"')
f.new_line('titlepage-text-color: "000000"')
f.new_line('titlepage-rule-color: "FFFFFF"')
f.new_line('titlepage-background: %s/resources/pagebackground.png' % os.path.split(src_dir)[0])
f.new_line('page-background: %s/resources/pagebackground.png' % os.path.split(src_dir)[0])
f.new_line('page-background-opacity: 1')
f.new_line('book: true')
f.new_line('classoption: oneside')
f.new_line('code-block-font-size: \scriptsize')
f.new_line('---')
f.new_line("\\pagebreak\n")

# Letter of Transmittal - See if you can make Heading 1 smaller
f.new_line(today.strftime("%B %d, %Y") + '\n')
f.new_line('Mr./Ms. %s' % main_poc['name'])
f.new_line(main_poc['job_title'])
f.new_line(client['full_name'] + '\n')
f.new_line('Dear Mr./Ms. %s,' % main_poc['name'])
f.new_paragraph('This report contains our findings and recommendations relating to the services performed for %s (%s) '
				'by the USC Security Operations (SecOps) Vulnerability Assessment and Penetration Team (VAPT).'
				% (client['full_name'], client['short_name']))
f.new_paragraph('Penetration testing is not intended to be an exhaustive test of all security controls nor does it '
				'provide any specific level of protection against possible breaches or compromise of systems, '
				'applications, and/or sensitive data. As a result, the projection of any conclusions, based on our '
				'testing, to future periods is subject to the risk that (1) changes are made to the systems or '
				'controls; (2) changes are required because of the regulations/compliance; or (3) new security '
				'vulnerabilities and exploits are discovered that may alter the validity of such conclusions. '
				'The USC SecOps VAPT performed the following services in the indicated timeframes. The findings '
				'included within this reports were identified as of the following specific periods in time:')

# Creates project table with start and end dates
project_table = ['Assessment Service', 'Start Date', 'End Date']
row_count = 1
f.new_line()
for key in reports:
	project_table.extend([reports[key]['project']['project_type'], reports[key]['project']['start_date'], reports[key]['project']['end_date']])
	row_count += 1
f.new_table(columns=3, rows=row_count, text=project_table, text_align='center')

f.new_line('This report is intended solely for use by the management of %s. We appreciate the '
		   'courtesies and cooperation extended to us during this project and the opportunity to be of '
		   'service to %s. Please contact Mr. Andy Portillo, Vulnerability Assessment Mananger, at '
		   'andy.portillo@usc.edu, or Mr. %s, Assessment Lead, at %s if you have any questions regarding this report.\n'
			% (client['short_name'], client['short_name'], lead['name'], lead['email']))
f.new_line('Very Respectfully,')
f.new_line(lead['name'])
f.new_line(lead['project_role'])
f.new_line(lead['email'])
f.new_line("\\pagebreak\n")

# Executive Summary
f.new_line('# Executive Summary\n')
f.new_line('## Background')
f.new_paragraph('Security of technology and information assets is an important priority within the University of '
				'Southern California. As threats to data and systems continuously evolve, so have the requirements '
				'for safeguarding information of our organization, students, staff, and faculty. The processes and '
				'people that support the security of technology are the key components in protecting these valuable '
				'assets. Likewise, it is important to measure the security of technological assets to understand the '
				'ability to defend against cyber threats.\n')
f.new_line('## Objective of the Vulnerability Assessment')
f.new_paragraph('The primary objectives of the engagment were to discover easily identifiable vulnerabilities on the '
				'external and internal presence of %s by providing the requested services. As part of the testing, the '
				'Vulnerability Assessment and Penetration Team (VAPT) conducted the following assessments:'
				% client['full_name'])

# Creates project list
projects = ""
for key in reports:
	projects += '* ' + reports[key]['project']['project_type'] + '\n'
	
f.new_paragraph(projects)
f.new_paragraph('The operators employed a multifaceted approach, including the use of automated and manual false-positive '
				' identification techniques to identify vulnerabilities.\n')
f.new_line('## Scope')
f.new_paragraph('The scope of the testing focused on the security controls of the external, internal, wireless network '
				'servers and their associated applications and operating systems. The web application testing focused on '
				'how securely the in-scope applications were developed and configured.')
f.new_paragraph('The operators were provided with the following in-scope targets for testing.\n')
				
# Creates list of in-scope targets
for key in reports:
	f.new_line('### ' + reports[key]['project']['project_type'])
	temp = ''
	temp = '* ' + reports[key]['project']['note'] + '\n'
	temp = re.sub(pattern, '', temp)
	temp = re.sub(bad_nl, '\n* ',temp)
	f.new_paragraph(temp)
	
f.new_line('## Results')
f.new_paragraph('The operators discovered the initial findings with non-credentialed and credentialed vulnerability scans. '
				'The operators then evaluated and inspected each of the discovered vulnerabilities from the vulnerability scan '
				'output to perform root cause analysis and research into possible recommendations to fix current issues and '
				'help prevent future occurrences.')
f.new_paragraph('The Technical Report section contains the findings that require attention to lower the risk of security compromises '
				'from occuring based on our review. For a full list of vulnerability scan results, please view the attached spreadsheet.\n')
f.new_line('### Summary of Strengths and Findings')
f.new_paragraph(summary + '\n')
f.new_line('### OCISO Assessment of %s Security' % client['short_name'])
f.new_paragraph('Based on a review of the results of our activities conducted within the allotted time frame, we believe, overall, your '
				'security ratings are as follows:')

# Creates risk assessment list
risk_assessment = ""
for key in reports:
	project_type = reports[key]['project']['project_type']
	if project_type == 'External Penetration Test':
		risk_assessment += '* External network: ' + risk_levels[key] + ' risk' + '\n'
	if project_type == 'Internal Penetration Test':
		risk_assessment += '* Internal network: ' + risk_levels[key] + ' risk' + '\n'
	if project_type == 'Wireless Network Assessment':
		risk_assessment += '* Wireless network: ' + risk_levels[key] + ' risk' + '\n'
	if project_type == 'Web Application Assessment':
		risk_assessment += '* Web Application: ' + risk_levels[key] + ' risk' + '\n'
	if project_type == 'Red Team Assessment':
		risk_assessment += '* Comprehensive Security: ' + risk_levels[key] + ' risk' + '\n'
	if project_type == 'Phishing Assessment':
		risk_assessment += '* Security Awareness: ' + risk_levels[key] + ' risk' + '\n'
f.new_paragraph(risk_assessment)

f.new_line('## Table of Summarized Findings\n')

# Creates table of summarized findings
for key in reports:
	f.new_line('### ' + reports[key]['project']['project_type'])
	project_table = ['Vulnerability', 'Severity']
	row_count = 1
	f.new_line()
	for key2 in reports[key]['findings']:
		project_table.extend([reports[key]['findings'][key2]['title'], reports[key]['findings'][key2]['severity']])
		row_count += 1
	f.new_table(columns=2, rows=row_count, text=project_table, text_align='center')

f.new_line('# Technical Report\n')

# THIS IS HOW THE SAUSAGE IS MADE
for key in reports:
	f.new_line('## ' + reports[key]['project']['project_type'] + ' (' + reports[key]['project']['codename'] + ')')
	for key2 in reports[key]['findings']:
		f.new_paragraph('### ' + reports[key]['findings'][key2]['title'])
		f.new_paragraph('**Severity: **' + reports[key]['findings'][key2]['severity'])

		f.new_paragraph('**Affected Entities:**')
		affected = ""
		affected = '* ' + reports[key]['findings'][key2]['affected_entities'] + '\n'
		affected = re.sub(pattern, '', affected)
		affected = re.sub(bad_nl, '\n* ', affected)
		f.new_paragraph(affected)
		
		f.new_paragraph('**Description: **' + re.sub(bad_nl, '', re.sub(pattern, '', reports[key]['findings'][key2]['description'])))
		f.new_paragraph('**Impact: **' + re.sub(bad_nl, '', re.sub(pattern, '', reports[key]['findings'][key2]['impact'])))
		f.new_paragraph('**Recommendation: **' + re.sub(bad_nl, '', re.sub(pattern, '', reports[key]['findings'][key2]['recommendation'])))
		f.new_paragraph('**Replication Steps: **' + re.sub(bad_nl, '', re.sub(pattern, '', reports[key]['findings'][key2]['replication_steps'])))
		
		f.new_paragraph('**Evidence:**')
		f.new_paragraph('>![' + reports[key]['findings'][key2]['evidence'][next(iter(reports[key]['findings'][key2]['evidence']))]['friendly_name'] + '](' + evidence_dir + '/' + key + '/' + os.path.split(reports[key]['findings'][key2]['evidence'][next(iter(reports[key]['findings'][key2]['evidence']))]['url'])[1] + ')')
		
		if reports[key]['findings'][key2]['host_detection_techniques']:
			f.new_paragraph('**Host Detection Techniques: **' + re.sub(bad_nl, '', re.sub(pattern, '', reports[key]['findings'][key2]['host_detection_techniques'])))
		if reports[key]['findings'][key2]['network_detection_techniques']:
			f.new_paragraph('**Network Detection Techniques: **' + re.sub(bad_nl, '', re.sub(pattern, '', reports[key]['findings'][key2]['network_detection_techniques'])))
		
		f.new_paragraph('**References: **')		
		references = ""
		references = '* ' + reports[key]['findings'][key2]['references'] + '\n'
		references = re.sub(pattern, '', references)
		references = re.sub(bad_nl, '\n* ', references)
		references = re.sub(r'\* \n', '', references)
		f.new_paragraph(references + '\n')
	f.new_line('\\pagebreak\n')

f.create_md_file()

os.system(cmd)

print('Done! Report has been generated at ' + os.path.split(src_dir)[0] + '/out/')
